<?php 
require "bdd/bddconfig.php";
ob_start();
session_start();  ?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div id="container">
    <div id="titre"><h1 class="centre">Livre d'or</h1></div>
    <div id="corps">
        <div id="questionnaire">
        
        <h1 class="centre">Ajouter votre message</h1>
    <form method="POST" action="insertmessage.php">
            Nom :<br/>
            <input type="text" name="Pseudo" value="" placeholder="Nom" required>
            <br />
            Message:<br>
            <textarea name="Message" rows="20" cols="80" required></textarea>
            <br/>
            <input type="submit" value="Envoyer">
    </form>
        
        </div>
        <div id="ancienMessage">
            <h1 class="centre">Les anciens messages</h1>
            <?php 
            try {
                $objBdd = new PDO("mysql:host=$bddserver;
               dbname=$bddname;
               charset=utf8",$bddlogin, $bddpass);
            
                $objBdd->setAttribute(PDO::ATTR_ERRMODE,
               PDO::ERRMODE_EXCEPTION);
            
               $listeMessages = $objBdd->query("SELECT * FROM message order by idMessage Desc limit 5");
            
               }
            catch (Exception $prmE) {
                die('Erreur : ' . $prmE->getMessage());
               } ?>
               <article>

        <?php
        //fetch = parcourir dans la fonction
        while ($messages = $listeMessages->fetch()) {
        //$bassin signifie que la fonction s'appel bassin, il faut toujours 
        //mettre $ avant une fonction
        ?>

        <h3><?php echo $messages['Pseudo']; ?></h3>
        <p><?php echo $messages['Message']; ?></p>
       <?php
        } //fin du while

        $listeMessages->closeCursor(); //libère les ressources de la bdd
        ?>

        </article>
        </div>
    </div>
    </div>

</body>
</html>